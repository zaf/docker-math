docker build  --target essential_builder -t essential_builder .
docker build  --target cuda_builder -t cuda_builder .
docker build  --target python_builder -t python_builder .

docker build  --target mpir_build -t mpir_build .
docker build  --target glpk_build -t glpk_build .
docker build  --target fortytwo_build -t fortytwo_build .
docker build  --target cdd_build -t cdd_build .
docker build  --target latte_int_build  -t latte_int_build  .
docker build  --target normaliz_build -t normaliz_build .
docker build  --target volume_build -t volume_build .
docker build  --target bazel_build -t bazel_build .

docker build  --target halcyon -t halcyon .
