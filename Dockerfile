

# To build the image you have to use nvidia-docker (v2)
# TODO:
# MPFR
# numpy
# flint
# ntl
# SCIP
# COCOA

##########################################################
# Base
##########################################################
# cuda-toolkit is not available in buster. use stretch
# https://packages.debian.org/search?keywords=nvidia-cuda-toolkit
# intel-mkl is not available in strecth but it avalable in buster.
# After upgrading to buster, remove the mkl download in tensoflow build
#

FROM debian:stable-slim as base
MAINTAINER Zafeirakis Zafeirakopoulos <zafeirakopoulos@gmail.com>

# General variables
ENV THEFLAGS="-march=native -mavx512f -mavx512er -mavx512cd -mavx512pf"
ENV BUILT_ARTICACTS=/output
ENV DEBIAN_FRONTEND noninteractive

# Versions of build tools
ENV BAZEL_VERSION=0.16.1

# Versions of libraries
ENV MPIR_VERSION=3.0.0
ENV CUDNN_VERSION=7.0.5.15-1+cuda9.0

# Versions of applications
ENV GLPK_VERSION=4.65
ENV NORMALIZ_VERSION=v3.6.3
ENV TENSORFLOW_VERSION=1.10
ENV FORTYTWO_VERSION=Release_1_6_8

##########################################################
# Essential builder
##########################################################
# This builder has no python, cuda, etc.
# It is used to build essential libraries, so that other
# images can take the artifacts from folder $BUILT_ARTICACTS
FROM base AS essential_builder

RUN apt-get update && apt-get install -yq --no-install-recommends \
    wget \
    bash \
    bzip2 \
    ca-certificates \
    sudo \
    locales \
    build-essential \
    m4 \
    yasm \
    cmake \
    git

RUN export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$BUILT_ARTICACTS

##########################################################
# MPIR builder
##########################################################
FROM essential_builder AS mpir_build

RUN wget http://mpir.org/mpir-$MPIR_VERSION.tar.bz2 && \
    tar -xvvf mpir-$MPIR_VERSION.tar.bz2

WORKDIR mpir-$MPIR_VERSION

RUN ./configure \
    --enable-gmpcompat \
    --prefix=$BUILT_ARTICACTS \
    --enable-cxx \
    CFLAGS="$THEFLAGS -O3 -fPIC"

#RUN make -j$(nproc)
#RUN cd tune && \
#    make tuneup
# Tuneup segfaults
#    ./tuneup > ../gmp-mparam.h

RUN make -j$(nproc) && \
    make -j$(nproc) check && \
    make install

##########################################################
# GLPK builder
##########################################################
FROM essential_builder AS glpk_build

RUN wget http://ftp.gnu.org/gnu/glpk/glpk-$GLPK_VERSION.tar.gz && \
    tar -xvvf glpk-$GLPK_VERSION.tar.gz

WORKDIR glpk-$GLPK_VERSION

RUN ./configure \
    --prefix=$BUILT_ARTICACTS \
    CFLAGS="$THEFLAGS -O3 -fPIC"

RUN make -j$(nproc) && \
    make -j$(nproc) check && \
    make install

##########################################################
# 4ti2 builder
##########################################################
FROM essential_builder AS fortytwo_build

COPY --from=mpir_build $BUILT_ARTICACTS $BUILT_ARTICACTS
COPY --from=glpk_build $BUILT_ARTICACTS $BUILT_ARTICACTS

RUN git clone --depth=1 --branch=$FORTYTWO_VERSION https://github.com/4ti2/4ti2.git

WORKDIR 4ti2

RUN ./configure \
    --prefix=$BUILT_ARTICACTS \
    --with-gmp=$BUILT_ARTICACTS \
    --with-glpk=$BUILT_ARTICACTS \
    CFLAGS="$THEFLAGS -O3 -fPIC"

#     make -j$(nproc) check  FAILS
RUN make -j$(nproc) && \
    make install-exec

##########################################################
# CDD builder
##########################################################
FROM essential_builder AS cdd_build

COPY --from=mpir_build $BUILT_ARTICACTS $BUILT_ARTICACTS

RUN wget https://www.math.ucdavis.edu/~latte/software/packages/cddlib/cddlib-0.94f+sage-patches-p8+latte-build-patch.tar.gz && \
    tar -xvvf cddlib-0.94f+sage-patches-p8+latte-build-patch.tar.gz

WORKDIR cddlib-0.94f+sage-patches-p8+latte-build-patch

RUN ./configure \
    --prefix=$BUILT_ARTICACTS \
    --with-gmp=$BUILT_ARTICACTS \
    CPPFLAGS='-I$BUILT_ARTICACTS:$CPPFLAGS' \
    LDFLAGS='-I$BUILT_ARTICACTS:$LDFLAGS' \
    CFLAGS="$THEFLAGS -O3 -fPIC"

RUN make -j$(nproc) && \
    make -j$(nproc) check && \
    make install


##########################################################
# Normaliz builder
##########################################################
FROM essential_builder AS normaliz_build
COPY --from=mpir_build $BUILT_ARTICACTS $BUILT_ARTICACTS

RUN git clone --depth=1 --branch=$NORMALIZ_VERSION https://github.com/Normaliz/Normaliz.git

#RUN git clone https://github.com/Normaliz/Normaliz.git && \
#    cd Normaliz && \
#    git checkout 52c6763b6a4ca51c479dd05d1eaff908cab4ba9b

RUN apt-get update && apt-get install -yq --no-install-recommends \
    libboost-dev

WORKDIR Normaliz

RUN mkdir BUILD

WORKDIR BUILD

RUN cmake  -DCMAKE_INSTALL_PREFIX=$BUILT_ARTICACTS \
    CMAKE_INCLUDE_PATH=$BUILT_ARTICACTS \
    CMAKE_LIBRARY_PATH=$BUILT_ARTICACTS \
     ../source && \
    make -j$(nproc) && \
    make install

RUN cp /usr/lib/gcc/x86_64-linux-gnu/6/libgomp.a $BUILT_ARTICACTS/lib/libgomp.a && \
    cp /usr/lib/gcc/x86_64-linux-gnu/6/libgomp.so $BUILT_ARTICACTS/lib/libgomp.so && \
    cp /usr/lib/gcc/x86_64-linux-gnu/6/libgomp.spec $BUILT_ARTICACTS/lib/libgomp.spec && \
    cp /usr/lib/x86_64-linux-gnu/libgomp.so.1 $BUILT_ARTICACTS/lib/libgomp.so.1 && \
    cp /usr/lib/x86_64-linux-gnu/libgomp.so.1.0.0 $BUILT_ARTICACTS/lib/libgomp.so.1.0.0


##########################################################
# Volume Approximation builder
##########################################################
FROM essential_builder AS volume_build

RUN mkdir /output && cd /output && mkdir bin && mkdir lib

WORKDIR /

RUN apt-get update && apt-get install -yq --no-install-recommends \
    libboost-dev \
    lp-solve && \
    git clone --depth=1 --branch=develop https://github.com/TolisChal/volume_approximation.git

#RUN git clone https://github.com/TolisChal/volume_approximation.git && \
#    cd volume_approximation && \
#    git checkout develop

WORKDIR volume_approximation/test

RUN mkdir BUILD

WORKDIR BUILD

RUN cmake  -DCMAKE_INSTALL_PREFIX=$BUILT_ARTICACTS .. && \
    make -j$(nproc)

RUN cp vol $BUILT_ARTICACTS/bin/vol && \ 
    cp /usr/lib/lp_solve/liblpsolve55.so $BUILT_ARTICACTS/lib/liblpsolve55.so && \
    cp /usr/lib/x86_64-linux-gnu/libcolamd.so.2 $BUILT_ARTICACTS/lib/libcolamd.so.2 && \
    cp /usr/lib/x86_64-linux-gnu/libcolamd.so.2.9.6 $BUILT_ARTICACTS/lib/libcolamd.so.2.9.6 && \
    cp /usr/lib/x86_64-linux-gnu/libsuitesparseconfig.so.4 $BUILT_ARTICACTS/lib/libsuitesparseconfig.so.4


##########################################################
##########################################################
##########################################################

##########################################################
# CUDA enabled builder
##########################################################
FROM essential_builder AS cuda_builder
# Is     gnupg2  needed?

RUN wget -nv -P /root/manual http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1704/x86_64/7fa2af80.pub \
   && echo "47217c49dcb9e47a8728b354450f694c9898cd4a126173044a69b1e9ac0fba96  /root/manual/7fa2af80.pub" | sha256sum -c --strict - \
   && apt-key add /root/manual/7fa2af80.pub \
   && wget -nv -P /root/manual http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1704/x86_64/cuda-repo-ubuntu1704_9.1.85-1_amd64.deb \
   && dpkg -i /root/manual/cuda-repo-ubuntu1704_9.1.85-1_amd64.deb \
   && rm -rf /root/manual \
   && apt-get update -qq \
   && apt-get install --no-install-recommends -y \
       cuda-toolkit-9-0 \
       libcudnn7=7.0.5.15-1+cuda9.0 \
       libcudnn7-dev=7.0.5.15-1+cuda9.0 \
       libcupti-dev \
       libnccl2 \
       libnccl-dev


##########################################################
# Builder with Python
##########################################################
FROM cuda_builder AS python_builder

RUN apt-get update && apt-get install -yq --no-install-recommends \
   python3-pip \
   python3-venv\
   python3-setuptools \
   python3-dev \
   python3-wheel

# Bazel (and maybe others) needs to find /usr/bin/env python
RUN cp /usr/bin/python3 /usr/bin/python

##########################################################
# Bazel builder
##########################################################
FROM python_builder AS bazel_build
RUN mkdir bazel
WORKDIR bazel
RUN wget https://github.com/bazelbuild/bazel/releases/download/$BAZEL_VERSION/bazel-$BAZEL_VERSION-dist.zip && \
    unzip bazel-$BAZEL_VERSION-dist.zip && \
    ./compile.sh && \
    cp /bazel/output/bazel $BUILT_ARTICACTS/bin/bazel
    # The Bazel binary is in /bazel/output/bazel


##########################################################
##########################################################
##########################################################
##########################################################


##########################################################
# Runtime
##########################################################
# Contains:
# LattE (TODO still from integrale)
# volume_approximation
# normaliz
# 4ti2
#
#
FROM base AS halcyon

COPY --from=mpir_build $BUILT_ARTICACTS /usr
COPY --from=glpk_build $BUILT_ARTICACTS /usr
COPY --from=fortytwo_build $BUILT_ARTICACTS /usr
COPY --from=cdd_build $BUILT_ARTICACTS /usr
COPY --from=volume_build $BUILT_ARTICACTS /usr
COPY --from=normaliz_build $BUILT_ARTICACTS /usr

# This is 143Mb of data. No need for budle.
COPY --from=latte_int_build $BUILT_ARTICACTS /usr

CMD ["bash"]


##########################################################
# Runtime with Jupyter
##########################################################
# Contains:
# LattE (TODO still from integrale)
# volume_approximation
# normaliz
# 4ti2
#
#
FROM base AS halcyon

COPY --from=mpir_build $BUILT_ARTICACTS /usr
COPY --from=glpk_build $BUILT_ARTICACTS /usr
COPY --from=fortytwo_build $BUILT_ARTICACTS /usr
COPY --from=cdd_build $BUILT_ARTICACTS /usr
COPY --from=volume_build $BUILT_ARTICACTS /usr
COPY --from=normaliz_build $BUILT_ARTICACTS /usr

# This is 143Mb of data. No need for budle.
COPY --from=latte_int_build $BUILT_ARTICACTS /usr

CMD ["bash"]








##########################################################
##########################################################
##########################################################
##########################################################

##########################################################
##########################################################
##########################################################


##########################################################
# Builder
##########################################################
# TODO:
# 1. Build numpy from scratch in a separate build stage (using mpir)

FROM base AS builder

COPY sources.list /etc/apt/sources.list

# Because openjdk-8-jdk tries to write in the man folder
RUN mkdir -p /usr/share/man/man1

RUN apt-get update && apt-get install -yq --no-install-recommends \
    wget \
    bash \
    bzip2 \
    ca-certificates \
    sudo \
    locales \
    python3-pip \
    python3-venv\
    python3-setuptools \
    python3-dev \
    python3-wheel \
    gnupg2 \
    build-essential \
    openjdk-8-jdk \
    zip \
    unzip \
    libgmp-dev \
    libgmp10\
    libgmp3-dev \
    libgmpxx4ldbl\
    libmpfr-dev \
    libmpfr4 \
    libmpfr4-dbg \
    libmpfrc++-dev \
    m4 \
    cmake \
    libgomp1 \
    git \
    python3-numpy


# No need if run by nvidia-docker
#RUN apt-get update && apt-get install -yq --no-install-recommends \
#    nvidia-driver



##########################################################
# numpy builder
##########################################################
FROM builder AS numpy_builder


##########################################################
# Tensorflow builder
##########################################################
FROM cuda_builder AS tensorflow_builder


COPY --from=bazel_builder /bazel//output/bazel /usr/local/bin/


# Do this instead:
#git clone --depth=1 --branch=r$TENSORFLOW_VERSION https://github.com/tensorflow/tensorflow.git .
RUN git clone https://github.com/tensorflow/tensorflow && \
    cd tensorflow && \
    git checkout r$TENSORFLOW_VERSION


WORKDIR /tensorflow

ENV PYTHON_BIN_PATH=/usr/bin/python
ENV PYTHON_LIB_PATH="$($PYTHON_BIN_PATH -c 'import site; print(site.getsitepackages()[0])')"
ENV PYTHONPATH=/tensorflow/lib
ENV PYTHON_ARG=/tensorflow/lib
ENV CUDA_TOOLKIT_PATH=/usr/local/cuda-9.0/
ENV CUDNN_INSTALL_PATH=/usr
ENV TF_NEED_GCP=0
ENV TF_NEED_CUDA=1
ENV TF_CUDA_VERSION=9.0
ENV TF_CUDA_COMPUTE_CAPABILITIES=6.1
ENV TF_NEED_HDFS=0
ENV TF_NEED_OPENCL=0
ENV TF_NEED_JEMALLOC=1
ENV TF_ENABLE_XLA=0
ENV TF_NEED_VERBS=0
ENV TF_CUDA_CLANG=0
ENV TF_CUDNN_VERSION=7
ENV TF_NEED_MKL=1
ENV TF_DOWNLOAD_MKL=1
ENV TF_NEED_AWS=0
ENV TF_NEED_MPI=0
ENV TF_NEED_GDR=0
ENV TF_NEED_S3=0
ENV TF_NEED_OPENCL_SYCL=0
ENV TF_SET_ANDROID_WORKSPACE=0
ENV TF_NEED_COMPUTECPP=0
ENV GCC_HOST_COMPILER_PATH=/usr/bin/gcc
ENV CC_OPT_FLAGS="-march=native"
ENV TF_NEED_KAFKA=0
ENV TF_NEED_TENSORRT=0
ENV GCC_HOST_COMPILER_PATH=/usr/bin/gcc
ENV CC_OPT_FLAGS="-march=native"
ENV TF_NCCL_VERSION=2.2
ENV TF_NCCL=/usr
ENV NCCL_INSTALL_PATH=/usr


# Bazel has hardocoded /lib/libnccl.so.2
RUN cp /usr/lib/x86_64-linux-gnu/libnccl.so.2 /usr/lib/

# https://github.com/NVIDIA/nvidia-docker/issues/374
RUN ln -s /usr/local/cuda/lib64/stubs/libcuda.so /usr/local/cuda/lib64/stubs/libcuda.so.1 && \
    export LD_LIBRARY_PATH=/usr/local/cuda/lib64/stubs/:$LD_LIBRARY_PATH && \
    bazel clean && \
    ./configure

# build TensorFlow (add  -s to see executed commands)
# "--copt=" can be "-mavx -mavx2 -mfma  -msse4.2 -mfpmath=both"
# build entire package
#
# Remove cuda flag because: WARNING: The following configs were expanded more than once: [cuda]. For repeatable flags, repeats are counted twice and may lead to unexpected behavior.
#RUN bazel build  -c opt --config=mkl --copt="-DEIGEN_USE_VML"  //tensorflow/tools/pip_package:build_pip_package
RUN bazel build --config=mkl --config=cuda --copt="-O3" --config=opt --copt="-DOMPI_SKIP_MPICXX" //tensorflow/tools/pip_package:build_pip_package

##########################################################
# JupyterLab builder
##########################################################
FROM builder AS jupyterlab_builder

##########################################################
# JupyterHub builder
##########################################################
FROM builder AS jupyterhub_builder



##########################################################
##########################################################
##########################################################
##########################################################



##########################################################
##########################################################
##########################################################
##########################################################



##########################################################
##########################################################
##########################################################
##########################################################


##########################################################
# Lidia builder
##########################################################
FROM essential_builder AS lidia_build

COPY --from=mpir_build $BUILT_ARTICACTS $BUILT_ARTICACTS

RUN wget https://www.math.ucdavis.edu/~latte/software/packages/lidia/current/lidia-2.3.0+latte-patches-2014-10-04.tar.gz && \
    tar -xvvf lidia-2.3.0+latte-patches-2014-10-04.tar.gz

WORKDIR lidia-2.3.0+latte-patches-2014-10-04

RUN ./configure \
    --prefix=$BUILT_ARTICACTS \
    --with-arithmetic=gmp \
    --with-extra-includes=$BUILT_ARTICACTS/include \
    --with-extra-libs=$BUILT_ARTICACTS/lib \
    --enable-shared=yes \
    CFLAGS="$THEFLAGS -O3 -fPIC"

RUN make -j$(nproc) && \
    make -j$(nproc) check && \
    make install-exec


##########################################################
# LattE Integrale builder
##########################################################
FROM essential_builder AS latte_int_build

RUN wget https://www.math.ucdavis.edu/~latte/software/packages/latte_current/latte-integrale-1.7.3b.tar.gz && \
    tar -xvvf latte-integrale-1.7.3b.tar.gz

WORKDIR latte-integrale-1.7.3b

RUN ./configure \
    --prefix=$BUILT_ARTICACTS \
    CFLAGS="$THEFLAGS -O3"

RUN make -j$(nproc) && \
    make -j$(nproc) check && \
    make install

##########################################################
# LattE builder
##########################################################
FROM essential_builder AS latte_build

COPY --from=mpir_build $BUILT_ARTICACTS $BUILT_ARTICACTS
COPY --from=cdd_build $BUILT_ARTICACTS $BUILT_ARTICACTS
COPY --from=fortytwo_build $BUILT_ARTICACTS $BUILT_ARTICACTS

COPY --from=latte_int_build $BUILT_ARTICACTS/share/lidia/ $BUILT_ARTICACTS/share/lidia/
COPY --from=latte_int_build $BUILT_ARTICACTS/lib/libLiDIA* $BUILT_ARTICACTS/lib/
COPY --from=latte_int_build $BUILT_ARTICACTS/include/lidia/ $BUILT_ARTICACTS/include/lidia/


RUN wget https://www.math.ucdavis.edu/~latte/software/packages/latte_current/latte-int-1.7.3.tar.gz && \
    tar -xvvf latte-int-1.7.3.tar.gz

WORKDIR latte-int-1.7.3

RUN echo $BUILT_ARTICACTS && \
    ./configure \
    --prefix=$BUILT_ARTICACTS \
    --enable-portable-binary \
    --disable-lidia \
    CFLAGS="$THEFLAGS -O3 -fPIC"

# Give the correct GMP
RUN make -j$(nproc) && \
    make -j$(nproc) check && \
    make install


# Crazy build system
##########################################################
# lpsolve builder
##########################################################
FROM essential_builder AS lpsolve_build

RUN wget https://sourceforge.net/projects/lpsolve/files/lpsolve/5.5.2.5/lp_solve_5.5.2.5_source.tar.gz/download

RUN tar -xvvf download

WORKDIR lp_solve_5.5

RUN ./configure \
    --prefix=$BUILT_ARTICACTS \
    CFLAGS="$THEFLAGS -O3"

RUN make -j$(nproc) && \
    make -j$(nproc) check && \
    make install




##########################################################
# NTL builder
##########################################################
FROM essential_builder AS ntl_build

COPY --from=mpir_build $BUILT_ARTICACTS $BUILT_ARTICACTS

RUN wget https://www.shoup.net/ntl/ntl-11.3.0.tar.gz && \
    tar -xvvf ntl-11.3.0.tar.gz

WORKDIR ntl-11.3.0

RUN ./configure \
    DEF_PREFIX=$BUILT_ARTICACTS \
    PREFIX=$BUILT_ARTICACTS \
    GMP_PREFIX=$BUILT_ARTICACTS \
    SHARED=on \
    NATIVE=on \
    TUNE=auto \
    NTL_STD_CXX14=on

RUN make -j$(nproc) && \
    make -j$(nproc) check && \
    make install
